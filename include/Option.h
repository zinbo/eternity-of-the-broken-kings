#include <string>
class State;
#ifndef OPTION_H
#define OPTION_H

using namespace std;

class Option
{
    public:
		string getDescription();
		string getConsequence();
		State* getNextState();
    protected:
    private:
		string description;
		string consequence;
		State* nextState;
};

#endif // OPTION_H
