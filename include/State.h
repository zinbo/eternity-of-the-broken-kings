#include <vector>
#include <string>
#include "Option.h"
#ifndef STATE_H
#define STATE_H

using namespace std;

class State
{
    public:
        State();
        virtual ~State();
        State clone();
        vector<Option> getOptions();
        string getDescription();
    protected:
    private:
        string description;
        vector<Option> options;
};

#endif // STATE_H
