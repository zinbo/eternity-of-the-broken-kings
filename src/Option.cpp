#include "State.h"

using namespace std;

string Option::getDescription(){
	return description;
}

string Option::getConsequence(){
	return consequence;
}

State* Option::getNextState(){
	return nextState;
}
